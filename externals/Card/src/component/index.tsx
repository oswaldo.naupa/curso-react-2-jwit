import styled from 'styled-components';
import React from 'react';
import Avatar from '@react/avatar';


const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 30px 15px;
  background-color: red;
  width: 123px;
  height: 160px;
  justify-content: center;
  align-items: center;
  border-radius: 6px;
`

const Title = styled.h3`
  color: white;
`

const Description = styled.p`
  font-size: 12px;
`

export interface params {
  value: string,
  title: string,
  description?: string,
  image: string,
  onClick?: (value: string) => void
}

/**
*
* @param params
* @params params.value identificador del elemento
* @params params.title titulo del elemento
* @params params.description descripcion del elemento
* @params params.image imagen del elemento
* @params params.onClick callback que returna un identificador al presionar sobre el componente
* 
* @example 
    import Card, { params as parmasCard} from "./components/Card"
    import Grid from "./components/Grid"
    import { useState } from 'react';

    const App= (): JSX.Element => {

      const [data, setData] = useState<parmasCard[]>([
        {
          value: "1",
          image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
          title: "Tienda 1"
        },
        {
          value: "2",
          image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
          title: "Tienda 2"
        },
        {
          value: "3",
          image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
          title: "Tienda 3" 
        },
      ])

      return (
        <Grid>
          {
            data.map((v, i)=> (<Card key={i} {...v} onClick=() />))
          }
        </Grid>
      )
    }

    export default App
* @return JSX.Element
*/

const App= (params:params): JSX.Element => {

  const handleClick = () => {
    if(typeof params.onClick === 'function') params.onClick(params.value)
  }

  return (
    <Container onClick={handleClick}>
        <Avatar image={params.image}/>
        <Title>{params.title}</Title>
        <Description>{params.description}</Description>
    </Container>
  )
}

export default App

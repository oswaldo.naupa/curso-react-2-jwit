import React from "react";
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 200px;
`

const Img = styled.img`
    object-fit: cover;
    aspect-ratio: 9/16;
`

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 10px;
  background-color: #1c1719;
  border-radius: 5px;
`

const Title = styled.h3`
  color: white;
`

const Description = styled.p`
  font-size: 12px;
`

export interface params {
  value:string,
  title: string,
  description?: string,
  image: string,
  onClick?: (value: string) => void,
  onMouseLeave?: React.MouseEventHandler<HTMLDivElement>,
  onMouseOver?: React.MouseEventHandler<HTMLDivElement>
}

const defaultProps: params = {
    value:"",
    image: "https://consumer.huawei.com/content/dam/huawei-cbg-site/latam/mx/mkt/plp/laptops/d14-amd-2021.jpg",
    title: "Product 1"
}

/**
*
* @param params
* @params params.value identificador del elemento
* @params params.title titulo del elemento
* @params params.description descripcion del elemento
* @params params.image imagen del elemento
* @params params.onClick callback que returna un identificador al presionar sobre el componente
* 
* @example 

  import CardProduct from "@react/product";

  const App= (): JSX.Element => {

    return (
        <CardProduct onMouseOver={()=>console.log("over")} onMouseLeave={()=>console.log("leave")} value="1" image="https://consumer.huawei.com/content/dam/huawei-cbg-site/latam/mx/mkt/plp/laptops/d14-amd-2021.jpg" title="Laptop"/>
    )
  }

  export default App
* @return JSX.Element
*/

const App= (params:params): JSX.Element => {

    const handleClick = () => {
        if(typeof params.onClick === 'function') params.onClick(params.value)
    }

    params = {...defaultProps, ...params}

    return (
        <Container onClick={handleClick} onMouseLeave={params.onMouseLeave} onMouseOver={params.onMouseOver}>
            <Img src={params.image}/>
            <Body>
                <Title>{params.title}</Title>
                <Description>{params.description}</Description>
            </Body>
        </Container>
    )
}

export default App

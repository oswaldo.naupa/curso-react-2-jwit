import React from "react";
import styled, { CSSProperties } from "styled-components";

const Container = styled.div`
    position: relative;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-self: center;
    width: 100%;
    height: 100%;
    overflow: auto;
    gap: 15px;
    //background-color: #222629;
`

interface params {
  children: React.ReactNode,
  style?: CSSProperties
}

const App = (params: params): JSX.Element => {

  
    return (
        <Container style={params.style}>
            {params.children}
        </Container>
    );

};

App.defaultProps = {
   
};

export default App;
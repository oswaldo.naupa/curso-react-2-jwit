import Card, { params as parmasCard} from "@react/card"
import Grid from "@react/grid"
import { useState } from 'react';

import CardProduct from "@react/product";

const App= (): JSX.Element => {

  const [data, setData] = useState<parmasCard[]>([
    {
      value: "1",
      image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
      title: "Tienda 1"
    },
    {
      value: "2",
      image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
      title: "Tienda 2"
    },
    {
      value: "3",
      image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
      title: "Tienda 3" 
    },
    {
      value: "4",
      image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
      title: "Tienda 4"
    },
    {
      value: "5",
      image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
      title: "Tienda 5"
    },
    {
      value: "6",
      image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
      title: "Tienda 6"
    },
    {
      value: "7",
      image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
      title: "Tienda 7"
    },
    {
      value: "8",
      image: "https://s1.eestatic.com/2020/05/18/como/gatos-mascotas-trucos_490961518_152142875_1706x960.jpg",
      title: "Tienda 8"
    },
  ])

  const handleSelect: parmasCard['onClick'] = (e) => {
    console.log(e);
  }

  return (
    <Grid>

      <CardProduct onMouseOver={()=>console.log("over")} onMouseLeave={()=>console.log("leave")} value="1" image="https://consumer.huawei.com/content/dam/huawei-cbg-site/latam/mx/mkt/plp/laptops/d14-amd-2021.jpg" title="Laptop"/>
      <CardProduct value="2" image="https://consumer.huawei.com/content/dam/huawei-cbg-site/latam/mx/mkt/plp/laptops/d14-amd-2021.jpg" title="Laptop"/>
      <CardProduct value="3" image="https://consumer.huawei.com/content/dam/huawei-cbg-site/latam/mx/mkt/plp/laptops/d14-amd-2021.jpg" title="Laptop"/>
      <CardProduct value="4" image="https://consumer.huawei.com/content/dam/huawei-cbg-site/latam/mx/mkt/plp/laptops/d14-amd-2021.jpg" title="Laptop"/>
      <CardProduct value="5" image="https://consumer.huawei.com/content/dam/huawei-cbg-site/latam/mx/mkt/plp/laptops/d14-amd-2021.jpg" title="Laptop"/>
      <CardProduct value="6" image="https://consumer.huawei.com/content/dam/huawei-cbg-site/latam/mx/mkt/plp/laptops/d14-amd-2021.jpg" title="Laptop"/>

      {
        data.map((v, i)=> (<Card key={i} {...v} onClick={handleSelect} />))
      }
    </Grid>
  )
}

export default App
